# Cours : PHP

## Sommaire

- [Cours : PHP](#cours--php)
  - [Sommaire](#sommaire)
  - [C'est quoi PHP ?](#cest-quoi-php-)
  - [Ou est-ce utilisé ?](#ou-est-ce-utilisé-)
  - [Installation d'un environnement serveur](#installation-dun-environnement-serveur)
  - [Notre première page web](#notre-première-page-web)
    - [Lancer le serveur sur Linux](#lancer-le-serveur-sur-linux)

## C'est quoi PHP ?

PHP: Hypertext Preprocessor, plus connu sous son sigle PHP, est un langage de programmation libre, principalement utilisé pour produire des pages Web dynamiques via un serveur HTTP, mais pouvant également fonctionner comme n'importe quel langage interprété de façon locale. PHP est un langage impératif orienté objet.

<div style="text-align: right">
    Source : 
    <a href="https://fr.wikipedia.org/wiki/PHP">Wikipedia</a>
</div>

## Ou est-ce utilisé ?

PHP est utilisé dans le monde entier, côté serveur : il est efficace. Rien à se reprocher, de plus la communauté PHP fourni énormément d'outil et de librairie open source.

Selon Wikipédia, PHP représente en 2013 plus de 244 millions de sites web à travers le monde.

| Année | Part de marché |
| --- | --- |
| 2010 | 75 % |
| 2013 | 75 % |
| 2016 | 82 % |

<div style="text-align: right">
    Source : 
    <a href="https://fr.wikipedia.org/wiki/PHP#Utilisation">Wikipedia</a>
</div>

## Installation d'un environnement serveur

PHP est un langage interprété, cela veut dire que vos fichiers PHP seront interprétés par du code C/C++ qui lui est compilé. 

Nous avons vu dans la partie "[Ou est-ce utilisé ?](#ou-est-ce-utilisé-)" que PHP est beaucoup utilisé côté serveur (Linux : debian, CentOS, Ubuntu ...). Mais beaucoup moins coté Windows ou OS X. 

C'est pour cela que nous aurons besoin de télécharger un logiciel intermédiaire nommé XAMPP qui va nous permettre de simuler un environnement serveur ([Apache](https://en.wikipedia.org/wiki/Apache_HTTP_Server), [MySQL](https://en.wikipedia.org/wiki/MySQL)/[MariaDB](https://en.wikipedia.org/wiki/MariaDB) et [PHP](https://en.wikipedia.org/wiki/PHP) dans ces différentes versions), le logiciel est disponible sur OS X, Linux et Windows. 

Pour télécharger XAMPP : https://www.apachefriends.org/fr/index.html

Note : L'installation se fait en deux étapes très simples pour les utilisateurs Linux. Une fois le fichier ".run" téléchargé, vous devez exécuter les deux commandes suivantes :

```console
$ # Rendre le fichier executable 
$ chmod +x xampp-linux-x64-[...]-installer.run

$ # Executer le fichier
$ sudo ./xampp-linux-x64-[...]-installer.run
```

Pour plus d'information sur l'installation, je vous invite à voir la partie "[How do I install XAMPP?](https://www.apachefriends.org/faq_linux.html)" disponible sur le site officiel de XAMPP.

## Notre première page web

Pour pouvoir créer notre premire page, vous devrez d'abord lancer l'environnement serveur fourni par XAMPP, cette environnement permettra de lancer un serveur web qui pourra interpretter vos requete et les faire passer dans votre fichier PHP.

### Lancer le serveur sur Linux

Par défaut Linux est lancé