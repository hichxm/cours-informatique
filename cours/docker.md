# Cours : Docker

## Sommaire

- [Cours : Docker](#cours--docker)
  - [Sommaire](#sommaire)
  - [Mise au point technique](#mise-au-point-technique)
    - [Différence entre conteneur / machine virtuelle](#différence-entre-conteneur--machine-virtuelle)
    - [C'est quoi Docker ?](#cest-quoi-docker-)
  - [Installation](#installation)
    - [Get Started : Debian (Linux)](#get-started--debian-linux)
    - [Lancer Docker sans les privilèges root.](#lancer-docker-sans-les-privilèges-root)
  - [Notre premier conteneur](#notre-premier-conteneur)
    - [Explication de la commande "docker run ..."](#explication-de-la-commande-docker-run-)
      - [Flag -i et -t](#flag--i-et--t)
    - [Exploiter notre conteneur debian](#exploiter-notre-conteneur-debian)
    - [Registry docker](#registry-docker)


## Mise au point technique

Tout d'abord avant de parler de Docker, il faut parler des conteneurs. J'ai trouvé un article sur internet résumant très bien la différence entre un conteneur et une machine virtuelle.

### Différence entre conteneur / machine virtuelle

> Avant d’aborder Docker, il est indispensable de rappeler ce qu’est une image container. Il s’agit d’un ensemble de processus logiciels léger et indépendant, regroupant tous les fichiers nécessaires à l’exécution des processus : code, runtime, outils système, bibliothèque et paramètres. 
> 
> [...] 
> 
> Alors que la virtualisation consiste à exécuter de nombreux systèmes d’exploitation sur un seul et même système, les containers se partagent le même noyau de système d’exploitation et isolent les processus de l’application du reste du système.
> 
> Pour faire simple, plutôt que de virtualiser le hardware comme l’hyperviseur, le container virtualise le système d’exploitation. Il est donc nettement plus efficient qu’un hyperviseur en termes de consommation des ressources système. 

<div style="text-align: right">
    Source : 
    <a href="https://www.lebigdata.fr/docker-definition#Quest-ce_quun_container
">LeBigData.fr</a>
</div>

### C'est quoi Docker ?

> Il s’agit d’une plateforme logicielle open source permettant de créer, de déployer et de gérer des containers d’applications virtualisées sur un système d’exploitation. Les services ou fonctions de l’application et ses différentes bibliothèques, fichiers de configuration, dépendances et autres composants sont regroupés au sein du container. Chaque container exécuté partage les services du système  d’exploitation.

<div style="text-align: right">
    Source : 
    <a href="https://www.lebigdata.fr/docker-definition#Docker_quest-ce_que_cest">LeBigData.fr</a>
</div>

## Installation

Pour installer Docker, rien de plus simple. Je vous invite fortement a suivre la procédure d'installation fourni par la documentation docker [disponible ici](https://docs.docker.com/get-docker/).

### Get Started : Debian (Linux)

Attention : Les lignes de code suivante peuvent ne plus être a jour. Donc veuilllez vérifier avec la documentation officiel.

```console
$ # Remove old docker version 
$ sudo apt-get remove docker docker-engine docker.io containerd runc

$ # Update apt package index and install package to use repository over HTTPs
$ sudo apt-get update && sudo apt-get install ca-certificates curl gnupg lsb-release

$ # Add official Docker's GPG key
$ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

$ # Add docker repository
$ echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

$ # Update apt package index
$ sudo apt-get update

$ # Install Docker Engine
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Maintenant il vous suffit de tester Docker

```console
$ sudo docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

```

Si vous avez ceci en retour, félicitation. Vous avez Docker sur votre machine

### Lancer Docker sans les privilèges root.

Si vous essayez de taper la commande "docker run hello-world" sans le prefix sudo, vous aurez un "permission denied". 

```console
$ docker run hello-world

docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/containers/create": dial unix /var/run/docker.sock: connect: permission denied.
See 'docker run --help'.
```

Vous avez cette erreur car vous n'êtes pas dans le groupe "docker".

```console
$ # Add current user to docker group
$ sudo usermod -aG docker $USER

$ # If command doesn't work, you need restart computer to refresh user's group list.
$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

[...]
```

## Notre premier conteneur

Pour commencer, nous allons lancer un conteneur avec la commande suivante : 

```console 
$ docker run -ti debian bash   

Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
0e29546d541c: Pull complete 
Digest: sha256:2906804d2a64e8a13a434a1a127fe3f6a28bf7cf3696be4223b06276f32f1f2d
Status: Downloaded newer image for debian:latest

$ root@f7ce72d526e7 :
```

Et la vous êtes dans un conteneur debian.

### Explication de la commande "docker run ..."

Avant de commencer nos premières manipulation avec Docker, nous allons expliquer la commande qu'on vient de taper dans le terminal.

```console
$ docker run --help

Usage:  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

Run a command in a new container

Options:

  [...]
  -t, --tty             Allocate a pseudo-TTY
  -i, --interactive     Keep STDIN open even if not attached
```

Lorsque l'on a tapé dans la console "docker run -ti debian bash". On a demandé à Docker d'éxécuter le conteneur debian avec les flags -t et -i. Et une fois le conteneur pret, éxécute la commande "bash"

#### Flag -i et -t

> The -t (or --tty) flag tells Docker to allocate a virtual terminal session within the container. This is commonly used with the -i (or --interactive) option, which keeps STDIN open even if running in detached mode (more about that later).

<div style="text-align: right">
    Source : 
    <a href="https://www.freecodecamp.org/news/docker-101-fundamentals-and-practice-edb047b71a51/">freeCodeCamp</a>
</div>

### Exploiter notre conteneur debian

Pour rappel nous avons tapé cette commande pour créer notre conteneur debian.

```console
$ docker run -ti debian bash 

$ root@5334df0ea1d9: 
```

Tout d'abord le code alphanuméric "5334df0ea1d9" represente l'identifiant du conteneur. Actuellement vous êtes connecté en tant que root sur la machine virtuelle debian.

Vous vous êtes demandé, ce que sa fait de supprimer absolument tout sur votre machine, vous pouvez le faire et sans rien craindre, nous somme dans un environement virtuelle.

```console
$ root@5334df0ea1d9: # Delete all file of your debian machine
$ root@5334df0ea1d9: rm -rf --no-preserve-root /

$ root@5334df0ea1d9: # Your linux is broken
$ root@5334df0ea1d9: ls
bash: ls: command not found
```

Pour quitter le terminal, il vous suffit de taper la commande "exit".

```console
$ root@5334df0ea1d9: # Exit shell session
$ root@5334df0ea1d9: exit

$ # Your terminal work classically
$ ls
```

Et vous pourrez créer une nouvelle machine avec la commande "docker run ..."

```console
$ docker run -ti debian bash

$ root@732b3ee9b2ba: 
```

Et hop ! Un nouvelle identifiant pour votre conteneur.

Lancons maintenant un conteneur ubuntu

```console
$ docker run -ti ubunu bash

Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
ea362f368469: Pull complete 
Digest: sha256:b5a61709a9a44284d88fb12e5c48db0409cfad5b69d4ff8224077c57302df9cf
Status: Downloaded newer image for ubuntu:latest

$ root@0578e9f7bbe2: 
```

Voila, vous venez de créer un conteneur ubuntu, vérifions maintenant que c'est bien un ubuntu.

```console
$ root@0578e9f7bbe2: # Show distrib
$ root@0578e9f7bbe2: cat /etc/*-release

DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04.3 LTS"
NAME="Ubuntu"
VERSION="20.04.3 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.3 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal
```

### Registry docker

Lorsque vous avez tapé la commande, vous avez sans doute du voir un téléchargement se dérouler. Ceci signifie que Docker télécharge l'image depuis la registry [Docker Hub](https://hub.docker.com/). Une registry docker est une bibliothèque ou il y a un catalogue d'images fourni par la communauté de l'open source.

Les images de [debian](https://hub.docker.com/_/debian) et d'[ubuntu](https://hub.docker.com/_/ubuntu) sont aussi disponible sur le Docker Hub.

Elles peuvent être compilé localement à partir d'un fichier Dockerfile ou être téléchargé à partir du Docker Hub (registry). Les images de debian et d'ubuntu sont elles aussi compilé à partir d'un fichier Dockerfile.