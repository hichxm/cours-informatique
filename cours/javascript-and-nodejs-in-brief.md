# Cours : PHP

## Sommaire

- [Cours : PHP](#cours--php)
  - [Sommaire](#sommaire)
  - [C'est quoi JavaScript ?](#cest-quoi-javascript-)
    - [On peut faire quoi avec JavaScript ?](#on-peut-faire-quoi-avec-javascript-)
      - [Cas d'usage](#cas-dusage)
  - [C'est quoi Node.js ?](#cest-quoi-nodejs-)
    - [On peut faire quoi avec Node.js ?](#on-peut-faire-quoi-avec-nodejs-)
      - [Cas d'usage](#cas-dusage-1)
    - [Utiliser NPM ou Yarn](#utiliser-npm-ou-yarn)
      - [À quoi sert NPM](#à-quoi-sert-npm)
      - [Et yarn ?](#et-yarn-)
      - [Exemple](#exemple)
        - [NPM](#npm)
        - [Yarn](#yarn)
  - [Notre premiere alerte JavaScript](#notre-premiere-alerte-javascript)

## C'est quoi JavaScript ?

> JavaScript (souvent abrégé en « JS ») est un langage de script léger, orienté objet, principalement connu comme le langage de script des pages web. Mais il est aussi utilisé dans de nombreux environnements extérieurs aux navigateurs web tels que Node.js, Apache CouchDB voire Adobe Acrobat. Le code JavaScript est interprété ou compilé à la volée (JIT). C'est un langage à objets utilisant le concept de prototype, disposant d'un typage faible et dynamique qui permet de programmer suivant plusieurs paradigmes de programmation : fonctionnelle, impérative et orientée objet.
>
> Source : [MDN Web Docs](https://developer.mozilla.org/fr/docs/Web/JavaScript)

### On peut faire quoi avec JavaScript ?

JavaScript est énormement utilisé dans le monde de tout les jours. Il dynamise les pages web permet une utilisation avancé de nos navigateurs web.

#### Cas d'usage

- Ajoute de l'intéraction dynamique à la page web
  - [Afficher ou cacher](https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp) des informations
  - [Changer la couleur d'un élément](https://www.w3schools.com/jsref/prop_style_color.asp) sans aucune action de l'utilisateur
  - Afficher un [`timer`](https://www.commentcoder.com/timer-javascript/) sur l'application
  - [Lire un fichier audio](https://stackoverflow.com/a/18628124/11836673) / [vidéo](https://stackoverflow.com/questions/21163756/html5-and-javascript-to-play-videos-only-when-visible)
- Créer un jeu vidéo 

## C'est quoi Node.js ?


> Node.js est une plateforme logicielle libre en JavaScript, orientée vers les applications réseau évènementielles hautement concurrentes qui doivent pouvoir monter en charge.
>
> [...]
> 
> Parmi les modules natifs de Node.js, on retrouve http qui permet le développement de serveur HTTP. Ce qui autorise, lors du déploiement de sites internet et d'applications web développés avec Node.js, de ne pas installer et utiliser des serveurs webs tels que Nginx ou Apache.
> 
> Concrètement, Node.js est un environnement bas niveau permettant l’exécution de JavaScript côté serveur.
>
> Source : [Wikpedia](https://fr.wikipedia.org/wiki/Node.js)

### On peut faire quoi avec Node.js ?

Node.js est donc une sorte de JavaScript avec des supers pouvoir, il propose une série de module et de package communautaire pouvant être exploité par votre système cette fois, et non votre navigateur web.

#### Cas d'usage

- [Création d'un chat en temps réel grâce aux websockets](https://www.cometchat.com/tutorials/how-to-build-a-chat-app-with-websockets-and-node-js)
- [Streaming vidéo / audio en temps réel](https://www.youtube.com/watch?v=lizjdKGQE_A)
- [Compiler du code JavaScript avec Babel](https://www.freecodecamp.org/news/setup-babel-in-nodejs/) pour les anciens navigateur et ancienne version de Node.js

### Utiliser NPM ou Yarn

#### À quoi sert NPM

> NPM (node package manager) est le gestionnaire de paquets par défaut pour l'environnement d'exécution JavaScript Node.js de Node.js.
>
> NPM se compose d'un client en ligne de commande, également appelé `npm`, et d'une base de données en ligne de paquets publics et privés payants, appelée le registre npm. Le registre est accessible via le client, et les paquets disponibles peuvent être parcourus et recherchés via le site Web de npm. Le gestionnaire de paquets et le registre sont gérés par npm, Inc.
>
> Source : [Wikipedia](https://fr.wikipedia.org/wiki/Npm)

#### Et yarn ?

Yarn fait exactement la même chose que NPM, à la différence prête. Il télécharge le paquet qu'une seule fois. NPM à la fâcheuse habitude de télécharger les packages directement dans le dossier `node_modules`. Alors que Yarn télécharge les packages dans un dossier dit de "cache", une fois le package téléchargé, un lien symbolique est créé entre le dossier `node_modules` et le dossier `cache` de Yarn.

#### Exemple

Malgrès une différence moindre (6 Mo), je vous laisse imaginer sur tout un dossier avec une trentaine de projet Node.js. La différence peut faire plusieurs Go.

> Note : Pour m'as part (le 14/01/2022), j'ai environ 80 projets (PHP, NodeJS et Golang) sur ma machine local pour un total de 32 Go. Sachant que j'utilise exclusivement l'outil Yarn. Les 32 Go sont exclusivement 32 Go de fichier, la plus part des 32 Go provient des dépendances de projet.

##### NPM

```console
root@00b88fe65125:/tmp/test-npm# ls -la
total 16
drwxr-xr-x  4 root root 4096 Jan 13 23:18 .
drwxrwxrwt  1 root root 4096 Jan 13 23:20 ..
drwxr-xr-x  8 root root 4096 Jan 13 23:16 homebridge
drwxr-xr-x 17 root root 4096 Jan 13 23:20 webpack

root@00b88fe65125:/tmp/test-npm# du -sh /tmp/test-npm/
584M	/tmp/test-npm/
```

##### Yarn

```console
root@61f78a250cd8:/tmp/test-yarn# ls -la
total 16
drwxr-xr-x  4 root root 4096 Jan 13 23:23 .
drwxrwxrwt  1 root root 4096 Jan 13 23:24 ..
drwxr-xr-x  8 root root 4096 Jan 13 23:24 homebridge
drwxr-xr-x 17 root root 4096 Jan 13 23:24 webpack

root@61f78a250cd8:/tmp/test-yarn# du -sh /tmp/test-yarn/
578M	/tmp/test-yarn/
```

## Notre premiere alerte JavaScript

JavaScript a la particularité de pouvoir facilement s'intégrer au navigateur.

En ouvrant votre inspecteur d'élément fourni par votre navigateur vous pourrez executer du JavaScript sur la page web en cour.

![Open inspector on chrome](../images/open-inspector-on-chrome.png)

Si on tape maintenant au niveau de la console un code, la page web réagira en direct au code intégrer à partir de la console développeur.

```javascript
alert('ok')
```

![Display alert in from console](../images/display-alert-from-chrome-console.png)