# L'ensemble des cours réalisé par Hicham SLIMANI

Soyez indulgent s'il vous plait.

## Cours

Listes des cours disponible

- [Langage PHP](cours/php.md)
- [Langage JavaScript et Node.js en bref](cours/javascript-and-nodejs-in-brief.md)
- [Docker](cours/docker.md)

## TP

Listes des TP disponibles

- [Laravel : Créer un URL shortener](tp/Laravel/create-url-shortener.MD)
- [Bash : Créer un script bash pour installer et configurer l'outil Swaks](tp/Shell/create-bash-script-to-install-swaks-with-credentials.md)