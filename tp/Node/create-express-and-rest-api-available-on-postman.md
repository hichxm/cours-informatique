# TP : Créer une API Rest avec Express et Mongoose

## Sommaire

- [TP : Créer une API Rest avec Express et Mongoose](#tp--créer-une-api-rest-avec-express-et-mongoose)
  - [Sommaire](#sommaire)
  - [Objectif du TP](#objectif-du-tp)
  - [Pour une base commune](#pour-une-base-commune)
    - [Base de donnée](#base-de-donnée)
  - [Aide](#aide)

## Objectif du TP

Nous voulons créer une API Rest avec Express et Mongoose. L'API Rest devra être exploitable à partir du [logiciel postman](https://www.postman.com/downloads/).

**Vous devez pour ce TP rendre la mise à jour d'un article possible**. Pour vous aider j'ai créé un fichier postman résumant l'ensemble des URL de notre application express. Il vous suffit de l'importer sur postman.

Le fichier est disponible ici : [postman_collection.json](https://gitlab.com/hichxm/express-with-socket.io-and-mongoose/-/blob/master/postman_collection.json)

> Note : Pensez à modifier les variables avec vos informations avant tout
> 
> ![edit postman variable first](./../../images/postman-collection-edit-collection-variable.png)

![postman collection select update article request](./../../images/postman-collection-select-article-request.png)

## Pour une base commune

Pour une base commune à tous, on prendra comme base le serveur express qu'on a réalisé ensemble disponible ici : https://gitlab.com/hichxm/express-with-socket.io-and-mongoose

> Note : Pensez à modifier les accès à la base de donnée avec les bonnes informations

### Base de donnée

La base de donnée MongoDB est disponible à l'addresse suivante :

```
mongodb://admin:admin@51.68.80.223:18149/express_app
```

La base de donnée est lancé via l'outil docker :
```console
$ docker run -d \
  -e MONGODB_INITIAL_PRIMARY_HOST="0.0.0.0" \
  -e MONGODB_ROOT_PASSWORD="yes" \
  -e MONGODB_DATABASE="express_app" \
  -e MONGODB_USERNAME="admin" \
  -e MONGODB_PASSWORD="admin" \
  -p 18149:27017 \
  bitnami/mongodb:latest
```

## Aide

Pour vous aider, j'ai mis en évidence le commit qui résume les changements réalisés pour créer la partie "article" de notre application.

[Le commit en question (le fichier server.js).](https://gitlab.com/hichxm/express-with-socket.io-and-mongoose/-/commit/54e57d0634da1428fa6d64324ff424cde0545e11?view=inline#dc60ea9ee1ff6246bf342463277d5b914db2883d)