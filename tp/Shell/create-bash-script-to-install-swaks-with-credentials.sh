#!/bin/bash

if [ "$EUID" -ne 0 ]
  then 
  echo "Please run with root permission : sudo" $0
  exit
fi

swaksConfigFile=$HOME/.swaksrc

rm $swaksConfigFile 
touch $swaksConfigFile

echo "Swaks installer by Hicham Slimani"
echo ""
echo "Swaks configuration file created : " $swaksConfigFile
echo ""
read -p "What's the SMTP host ? : " smtpHost
read -p "What's the SMTP port ? : " smtpPort
read -p "What's the SMTP username ? : " smtpUser
read -sp "What's the SMTP password ? : " smtpPass
read -p "What's the SMTP encryption ? (ssl, tls or none) : " smtpEncryption
echo ""
read -p "Default sender address ? : " defaultSenderAddress
echo ""

echo "--server" $smtpHost >> $swaksConfigFile
echo "--port" $smtpPort >> $swaksConfigFile
echo "-au" $smtpUser >> $swaksConfigFile
echo "-ap" $smtpPass >> $swaksConfigFile
echo "--from" $defaultSenderAddress >> $swaksConfigFile

if [ "$smtpEncryption" == "tls" ] || [ "$smtpEncryption" == "ssl" ]
then
  echo "-"$smtpEncryption >> $swaksConfigFile
fi

echo "Installing swaks from APT package manager"

apt-get -qq update && apt-get install swaks -yqq

echo "Swaks is ready!"
echo "You can try swaks with following line :"
echo ""
echo "  swaks --to" $defaultSenderAddress
echo ""
echo "To get help :"
echo ""
echo "  man swaks"
echo ""
echo "Enjoy."