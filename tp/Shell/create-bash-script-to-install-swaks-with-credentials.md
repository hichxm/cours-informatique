# TP : Créer un script bash pour installer et configurer l'outil Swaks

## Sommaire

- [TP : Créer un script bash pour installer et configurer l'outil Swaks](#tp--créer-un-script-bash-pour-installer-et-configurer-loutil-swaks)
  - [Sommaire](#sommaire)
  - [Objectif du TP](#objectif-du-tp)

## Objectif du TP

Le but du TP est d'installer et de configurer l'outil [Swaks](http://www.jetmore.org/john/code/swaks/) à l'aide d'un script. On voudra un outil simple à utiliser. **L'outil doit parfaitement marcher sur n'importe quelle distribution debian**.

> Note : Le fichier `$HOME/.swaksrc` est la configuration par défaut lorsque l'utilisateur lance la commande swaks, l'outil chargera le fichier `.swaksrc`. Pour plus d'information je vous invite à voir la partie "[OPTION PROCESSING > CONFIGURATION FILES](https://github.com/jetmore/swaks/blob/v20201014.0/doc/base.pod#configuration-files)" de la documentation officiel.

```console
$ sudo ./swaks-installer.sh

Swaks installer by Hicham Slimani

Swaks configuration file created : /home/hichxm/.swaksrc

What's the SMTP host ? : smtp.google.com
What's the SMTP port ? : 587
What's the SMTP username ? : hicham.slimani@digityourdream.fr
What's the SMTP password ? : my-real-password-you-can-try-it
What's the SMTP encyption ? (ssl, tls or none) : tls

Default sender address : hicham.slimani+courses@digityourdream.fr

Installing swaks from APT package manager

[..]

Swaks is ready!
You can try swaks with following line :
  
  swaks --to hicham.slimani+courses@digityourdream.fr

To get help :
  
  man swaks

Enjoy.
```

## Solution

La solution est disonible ici : [create-bash-script-to-install-swaks-with-credentials.sh](create-bash-script-to-install-swaks-with-credentials.sh)
