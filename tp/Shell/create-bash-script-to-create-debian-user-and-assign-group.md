# TP : Créer un script Bash dans le but de créer un utilisateur et de lui assigner un groupe d'utilisateur

## Sommaire

- [TP : Créer un script Bash dans le but de créer un utilisateur et de lui assigner un groupe d'utilisateur](#tp--créer-un-script-bash-dans-le-but-de-créer-un-utilisateur-et-de-lui-assigner-un-groupe-dutilisateur)
  - [Sommaire](#sommaire)
  - [Objectif du TP](#objectif-du-tp)
    - [Exemple](#exemple)
  - [Aide](#aide)

## Objectif du TP

Nous souhaitons avoir un script qui va nous permettre de créer un utilisateur et de lui assigner un groupe d'utilisateur.

### Exemple

```
Ce script vous permettra de créer un utilisateur et lui assigner un role

Qu'elle est le nom de l'utilisateur ? : hichxm

Définir un mot de passe ? (si non, laissez vide) : ******

Liste des groupes disponible sur la machine :

[1] Adminstrateur
[2] Techincien
[3] Professeur
[4] Etudiant

Qu'elle groupe assigné ? : 3

Utilisateur "hichxm" crée.
Définition du mot de passe.
Attribution du groupe "Professeur" à "hichxm".
```

## Aide

- [Bash Scripting Tutorial - 3. User Input](https://ryanstutorials.net/bash-scripting-tutorial/bash-input.php)
- [useradd(8) - Linux man page](https://linux.die.net/man/8/useradd)
- [groups(1) - Linux man page](https://linux.die.net/man/1/groups)