# TP : Déployer l'application POLR sur un réseau docker swarm

## Sommaire

- [TP : Déployer l'application POLR sur un réseau docker swarm](#tp--déployer-lapplication-polr-sur-un-réseau-docker-swarm)
  - [Sommaire](#sommaire)
  - [Objectif](#objectif)
    - [Pour vous aider](#pour-vous-aider)
  - [Bonus](#bonus)
    - [Utiliser les secrets](#utiliser-les-secrets)
    - [Sauvegarder la base de données](#sauvegarder-la-base-de-données)

## Objectif

L'objectif de ce TP sera de déployer un conteneur de l'application polr. Cette application devra être déployée sur un cluster docker swarm. Notre cluster docker swarm devra être géré par l'outil [`portainer`](https://www.portainer.io/).

> Polr is a quick, modern, and open-source link shortener. It allows you to host your own URL shortener, to brand your URLs, and to gain control over your data
> 
> Source : [polr's website](https://polrproject.org/)

### Pour vous aider

Pour vous aider, vous avez ci-dessous la commande pour lancer un conteneur polr. À vous maintenant de la transformer pour qu'elle soit adaptée à notre fichier de configuration docker swarm ainsi que le [repository GIT](https://gitlab.com/hichxm/full-website-with-containers) résumant les fichiers de configuration d'un autre projet dockerizé.

```bash
$ docker run -p 80:8080 \
    -e "DB_HOST=mysql.database-cluster.com" \
    -e "DB_DATABASE=polr" \
    -e "DB_USERNAME=super-username" \
    -e "DB_PASSWORD=super-password" \
    
    -e "APP_ADDRESS=my-shortener-url.net" \
    
    -e "ADMIN_USERNAME=admin" \
    -e "ADMIN_PASSWORD=very-long-and-secret-password" \
    
    -e "POLR_SETUP_RAN=true" \
    -e "SETTING_PUBLIC_INTERFACE=true" \
    -e "SETTING_SHORTEN_PERMISSION=false" \
    -e "POLR_BASE=32" \
    ajanvier/polr
```

## Bonus

### Utiliser les secrets

L'utilisation des `secrets` avec docker swarm sera un plus pour ce TP.

### Sauvegarder la base de données

Essayer d'enregistrer les données liées à notre base de données grâce aux `volumes`. Actuellement si vous venez à relancer le service en rapport avec la base de données, les données ne seront pas synchronisée.