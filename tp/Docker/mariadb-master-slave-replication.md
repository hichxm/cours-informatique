# TP : Déployer une base de données MariaDB avec un maitre et un esclave

## Sommaire

- [TP : Déployer une base de données MariaDB avec un maitre et un esclave](#tp--déployer-une-base-de-données-mariadb-avec-un-maitre-et-un-esclave)
  - [Sommaire](#sommaire)
  - [Objectif](#objectif)
    - [Pour vous aider](#pour-vous-aider)

## Objectif

L'objectif de ce TP est d'établir une base de données MariaDB principale (maitre) et une seconde base de données (esclave) afin d'avoir une redondance des données et par la même occasion proposée de la haute disponibilité.

Le concept parait très complexe. En réalité, il est très simple.

### Pour vous aider

 - [mariadb.com - Setting Up Replication](https://mariadb.com/kb/en/setting-up-replication/)
 - [hub.docker.com - MariaDB](https://hub.docker.com/_/mariadb)

