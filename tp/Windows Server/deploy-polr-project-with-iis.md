# TP : Déployer l'application Polr sur un serveur web IIS

## Sommaire

- [TP : Déployer l'application Polr sur un serveur web IIS](#tp--déployer-lapplication-polr-sur-un-serveur-web-iis)
  - [Sommaire](#sommaire)
  - [Objectif](#objectif)
    - [Pour vous aider](#pour-vous-aider)
    - [Bonus](#bonus)

## Objectif

L'objectif du jour sera de déployer l'application Polr sur un windows serveur via l'outil IIS. L'application Polr aura besoin d'une base de données SQL, nous utiliserons donc une base de données MySQL 8.

L'application Polr : https://github.com/cydrobolt/polr

### Pour vous aider

Il y a une série de vidéo à voir poue vous aider : 

 - [Installer Windows Server 2019](https://www.youtube.com/watch?v=ivm4FocrsgA)
   - [Image ISO Windows Server 2019](https://lecrabeinfo.net/telecharger/windows-server-2019-x64)
 - [Comment installer ou configurer le serveur IIS (serveur Web) dans Windows Server 2019](https://www.youtube.com/watch?v=eeBm2H1Yuok)
 - [Installation de PHP sur IIS à l'aide du programme d'installation de la plate-forme Web](https://www.youtube.com/watch?v=q9QlVoInnG8)
   - [PHP Manager : Des fois il ne voudra pas s'installer à partir du gestionnaire de role (erreur 404 sur le C++ redistribuable)](https://github.com/phpmanager/phpmanager/releases)
 - [Installing MySQL Server on Windows Server 2019](https://www.youtube.com/watch?v=n2zZe5AIagg)
   - [MySQL installer](https://dev.mysql.com/downloads/installer/) 

PHP est disponible sur le `cmd.exe` via la commande `php`. Vous pourrez donc installer l'outil [`composer`](https://getcomposer.org/) via l'invité de commande.

### Bonus

Si vous avez réussi à installer l'application Polr, je vous invite à installer PhpMyAdmin et de créer deux sous domaines `phpmyadmin.my-domain.lan` et `polr.my-domain.lan` accessibla via notre réseau local.