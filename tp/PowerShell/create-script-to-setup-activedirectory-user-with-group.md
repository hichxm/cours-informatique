# TP : Créer un script Power Shell dans le but de créer un utilisateur et de lui assigner un groupe d'utilisateur

## Sommaire

- [TP : Créer un script Power Shell dans le but de créer un utilisateur et de lui assigner un groupe d'utilisateur](#tp--créer-un-script-power-shell-dans-le-but-de-créer-un-utilisateur-et-de-lui-assigner-un-groupe-dutilisateur)
  - [Sommaire](#sommaire)
  - [Objectif du TP](#objectif-du-tp)
    - [Exemple](#exemple)
  - [Aide](#aide)

## Objectif du TP

Nous souhaitons avoir un script qui va nous permettre de créer un utilisateur et de lui assigner un groupe d'utilisateur.

### Exemple

```
Ce script vous permettra de créer un utilisateur et lui assigner un role

Qu'elle est le nom de l'utilisateur ? : hichxm

Définir un mot de passe ? (si non, laissez vide) : ******

Liste des groupes disponible sur l'AD :

[1] Adminstrateur
[2] Techincien
[3] Professeur
[4] Etudiant

Qu'elle groupe assigné ? : 3

Utilisateur "hichxm" crée.
Définition du mot de passe.
Attribution du groupe "Professeur" à "hichxm".
```

## Aide

- [ActiveDirectory Module - Microsoft Documentation](https://docs.microsoft.com/en-us/powershell/module/activedirectory)
  - [New-AddUser - Microsoft Documentation](https://docs.microsoft.com/en-us/powershell/module/activedirectory/new-aduser)
  - [Add-AddGroupMember - Microsoft Documentation](https://docs.microsoft.com/en-us/powershell/module/activedirectory/add-adgroupmember)
- [Read-Host - Microsoft Documentation](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/read-host)