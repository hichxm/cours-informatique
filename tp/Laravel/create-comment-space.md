# Solution : TP : Mise en place d'un espace commentaire avec Laravel

## Sommaire

- [Solution : TP : Mise en place d'un espace commentaire avec Laravel](#solution--tp--mise-en-place-dun-espace-commentaire-avec-laravel)
  - [Sommaire](#sommaire)
  - [Objectif du TP](#objectif-du-tp)
    - [Pour vous aider](#pour-vous-aider)

## Objectif du TP

L'objectif du TP sera de créer un espace commentaire sur la page d'un article.
Le projet est disponible via `PHP 8.0` à l'adresse suivante :

**Pensez à modifier le fichier `.env` avec vos informations de base de donnée**

```
git clone git@gitlab.com:hichxm/cours-create-web-app.git
```

Une fois le fichier `.env` set up. Pensez à installer les dépendances et à effectuer les migrations.

```
composer install
php artisan migrate
```

### Pour vous aider

 - [Laravel : Créer le CRUD d'un blog](https://www.akilischool.com/cours/laravel-crud-avec-upload-dimage)
 - [Créer un forum avec Laravel 5.8: CRUD des topics](https://www.youtube.com/watch?v=sNfSAP2sH9Q)
 - [Laravel 8 - CRUD application tutorial with Example](https://www.laravelcode.com/post/laravel-8-crud-application-tutorial-with-example)
