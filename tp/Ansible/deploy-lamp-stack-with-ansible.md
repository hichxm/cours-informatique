# TP : Déployer une LAMP stack grâce à Ansible

## Sommaire

- [TP : Déployer une LAMP stack grâce à Ansible](#tp--déployer-une-lamp-stack-grâce-à-ansible)
  - [Sommaire](#sommaire)
  - [Objectif](#objectif)
    - [Pour vous aider](#pour-vous-aider)

## Objectif

L'objectif du TP est de déployer une stack **LAMP (*Linux Apache MySQL PHP*)** sur un serveur grâce à l'outil Ansible. Il faudra déployer une application PHP de votre choix (un simple phpinfo() fera l'affaire).

Vous devrez ensuite être capable de modifier la configuration Apache pour qu'il pointe vers le nom de domaine "mon-super-projet-avec-ansible.local", ensuite modifier votre fichier `hosts` pour qu'il pointe vers la bonne adresse IP.

### Pour vous aider

  - [How to create a LAMP stack with Ansible](https://coderwall.com/p/6zm8rq/how-to-create-a-lamp-stack-with-ansible)
  - [Création de notre playbook Ansible (stack LAMP)](https://devopssec.fr/article/creation-playbook-ansible-stack-lamp)